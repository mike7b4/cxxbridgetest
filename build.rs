fn main() {
    cxx_build::bridge("src/ffi.rs")
        .file("src/main.cc")
        .flag_if_supported("-std=c++17")
        .compile("cxx-demo");
}
