use serde::Serialize;
use std::io::{Error, ErrorKind};
use std::str::FromStr;
#[cxx::bridge]
mod cli {
    extern "Rust" {
        type MacAddress;
        fn mac_new(s: &str) -> Box<MacAddress>;
    }
}

#[derive(Serialize)]
struct MacAddress {
    inner: [u8; 6],
}

impl FromStr for MacAddress {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.replace(':', "");
        if s.len() != 12 {
            return Err(Error::new(
                ErrorKind::Other,
                format!("{} is not a MAC address", s),
            ));
        }
        Ok(MacAddress {
            inner: [1, 2, 3, 4, 5, 6],
        })
    }
}

fn mac_new(s: &str) -> Box<MacAddress> {
    Box::new(MacAddress::from_str(s).unwrap())
}
